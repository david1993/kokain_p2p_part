#pragma once

#include <string>
#include <QHostAddress>
#include <QTcpSocket>

namespace Kokain {
namespace Common {

enum class RequestType : char
{
    Undefined = -1,
    Register = 0,
    Connect,
    Unregister
};

struct client
{
    client(const std::string& email,
           const QHostAddress& publicIP,
           const quint16 publicPort,
           QTcpSocket* socket)
        : m_email(email),
          m_publicIP(publicIP),
          m_publicPort(publicPort),
          m_socket(socket)

    {}

    std::string     m_email;
    QHostAddress    m_publicIP;
    quint16         m_publicPort;
    QTcpSocket* m_socket;
};

struct responce
{
    responce()
    {}

    responce(const bool exists, const std::string& email, const QHostAddress ip, const quint16 port)
        : m_exists(exists),
          m_email(email),
          m_ip(ip),
          m_port(port)
    {}

    bool m_exists;
    std::string m_email;
    QHostAddress m_ip;
    quint16 m_port;
};

}}
