#-------------------------------------------------
#
# Project created by QtCreator 2015-07-19T16:58:03
#
#-------------------------------------------------

QT       += core network

QT       -= gui

TARGET = Kokain_Server
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app
QMAKE_CXXFLAGS += -std=c++0x

SOURCES += main.cpp \
    server.cpp

HEADERS += \
    common.h \
    server.h
