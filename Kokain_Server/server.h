#ifndef SERVER_H
#define SERVER_H

#include <QTcpServer>

#include "common.h"

typedef std::map<std::string, Kokain::Common::client> Clients;

class Server : public QTcpServer
{
    Q_OBJECT
public:
    static Server*  GetInstance();
    static void     RemoveInstance();

protected:
    void incomingConnection(qintptr handle);
    void typeServerPortAndIP();
    bool startServer(const qint16&);
    void onRegister(const std::string& email, Kokain::Common::client usr);

private slots:
    void onReadyRead();
    void onConnected();

private:
    Kokain::Common::RequestType check_package_validity(QDataStream&, const QTcpSocket* socket);

private:
    static Server*  m_instance;
    Clients         m_clients;

private:
    explicit Server(QObject *parent = 0);
    virtual ~Server();

    Server(const Server&)               = delete;
    Server& operator=(const Server&)    = delete;
};

#endif // SERVER_H
