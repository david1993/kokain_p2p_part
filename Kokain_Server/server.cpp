#include "server.h"

#include <iostream>

#include <QTcpSocket>
#include <QHostAddress>
#include <QDataStream>

#ifndef min
#define min(a,b) (((a) < (b)) ? (a) : (b))
#endif

Server* Server::m_instance = nullptr;

Server* Server::GetInstance()
{
    if (!m_instance)
        m_instance = new Server();
    return m_instance;
}

void Server::RemoveInstance()
{
    delete m_instance;
}

Server::Server(QObject *parent) :
    QTcpServer(parent)
{
    std::cout << "Welcome to Kokain Server" << std::endl;
    typeServerPortAndIP();
}

Server::~Server()
{}

void Server::typeServerPortAndIP()
{
    startServer(9779);
}

void Server::incomingConnection(qintptr descriptor)
{
    QTcpSocket* pTcpSocket = new QTcpSocket(this);
    if (pTcpSocket->setSocketDescriptor(descriptor))
    {
        std::cout << "Incoming Connection..." << std::endl;

        connect(pTcpSocket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
        connect(pTcpSocket, SIGNAL(connected()), this, SLOT(onConnected()));
    }
}

void Server::onReadyRead()
{
    std::cout << "Ready Read" << std::endl;
    QTcpSocket* socket = dynamic_cast<QTcpSocket*>(QObject::sender());
    if (!socket) {
        std::cout << "Bad Cast" << std::endl;
        return;
    }

    QDataStream in(socket);
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);

    Kokain::Common::RequestType requestType;
    requestType = check_package_validity(in, socket);

    if (requestType == Kokain::Common::RequestType::Undefined) {
        return;
    }

    QHostAddress    publicIP    = socket->peerAddress();
    quint32         publicPort  = socket->peerPort();
    std::string     email;
    {
        qint8 character;
        do {
            in >> character;
            email.push_back(character);
        } while (character != '\0');

//        while (socket->bytesAvailable())
//        {
//            in >> character;
//        }
    }

    switch (requestType) {
    case Kokain::Common::RequestType::Register:
    {
        qint8 character;
        while (socket->bytesAvailable())
        {
            in >> character;
        }
        m_clients.insert(std::make_pair(email, Kokain::Common::client(email, publicIP, publicPort, socket)));
        const auto it = m_clients.find(email);
        std::cout << "Port: " << it->second.m_publicPort << std::endl;
        std::cout << "Client " << email << " has just connected." << std::endl;
        std::cout << "IP: " << publicIP.toIPv4Address() << std::endl;
        std::cout << "Port: " << publicPort << std::endl;

        qint32 ip = publicIP.toIPv4Address();
        qint16 port = publicPort;

        out << ip;
        out << port;
        socket->write(block);
        break;
    }
    case Kokain::Common::RequestType::Unregister:
    {
        qint8 character;
        while (socket->bytesAvailable())
        {
            in >> character;
        }
        const auto it = m_clients.find(email);
        if (it == m_clients.end()) {
            std::cout << "Client not found" << std::endl;
            return;
        }
        if ((*it).second.m_publicIP == publicIP &&
                (*it).second.m_publicPort == publicPort) {
            std::cout << "Client " << it->second.m_email << "is removed" << std::endl;
            m_clients.erase(it);           
        }
        else {
            QTcpSocket* verificationSocket = new QTcpSocket(this);
            int port = (*it).second.m_publicPort;
            qDebug() << port << " " << (*it).second.m_publicPort;
            verificationSocket->connectToHost((*it).second.m_publicIP, (*it).second.m_publicPort);
            if (!verificationSocket->waitForConnected(3000)) {
                m_clients.erase(it);
            }
        }
        break;
    }
    case Kokain::Common::RequestType::Connect:
    {
        Kokain::Common::responce resp;

        const auto it = m_clients.find(email);

        if (it == m_clients.end()) {
            resp.m_exists = false;
            char* data = new char[39];
            *data = 0;
            socket->write(data, 39);
            delete data;
            data = nullptr;
            std::cout << "Client " << email << " not found" << std::endl;
        }
        else {
            std::string     req_email;
            {
                qint8 character;
                do {
                    in >> character;
                    req_email.push_back(character);
                } while (character != '\0');

                while (socket->bytesAvailable())
                {
                    in >> character;
                }
            }

            std::cout << "Connect reques sent to " << email << "IP: " << it->second.m_publicIP.toString().toStdString()
                      << "Port: " << it->second.m_publicPort << " \nFrom " << req_email << "IP: " << publicIP.toString().toStdString() << "Port: " << publicPort << std::endl;

            resp.m_exists = true;
            resp.m_email = email;
            resp.m_ip = it->second.m_publicIP;
            resp.m_port = it->second.m_publicPort;

            char* data = new char[33];
            data[0] = 1;
            memcpy(data + 1, &resp.m_email[0], min(resp.m_email.size() + 1, 32));
            qint32 ip = resp.m_ip.toIPv4Address();
            qint16 port = resp.m_port;

            socket->write(data, 33);
            out << ip;
            out << port;
            socket->write(block);
            delete data;
            data = nullptr;

            data = new char[33];
            data[0] = 1;
            memcpy(data + 1, &req_email[0], min(req_email.size() + 1, 32));
            ip = publicIP.toIPv4Address();
            port = publicPort;

            it->second.m_socket->write(data, 33);

            QByteArray block_1;
            QDataStream out_1(&block_1, QIODevice::WriteOnly);
            out_1 << ip;
            out_1 << port;
            it->second.m_socket->write(block_1);
            delete data;
            data = nullptr;

            std::cout << "Client " << email << " found." << std::endl;
        }

        break;
    }
    default:
    {
        std::cerr << "Undefined Request Type." << std::endl;
    }
    }
}

void Server::onConnected()
{
    std::cout << "Connected." << std::endl;
}

void Server::onRegister(const std::string& email, Kokain::Common::client usr)
{
    m_clients.insert(std::make_pair(email, usr));
}

bool Server::startServer(const qint16 & port)
{
    if (!listen(QHostAddress::Any, port))
    {
        std::cout << "Server not started at: " << port << std::endl;
        return false;
    }
    std::cout << "Server started at: " << port << std::endl;
    return true;
}

Kokain::Common::RequestType Server::check_package_validity(QDataStream& in, const QTcpSocket* socket)
{
    size_t package_size = socket->bytesAvailable();
    if (package_size != 33) {
        return Kokain::Common::RequestType::Undefined;
    }

    Kokain::Common::RequestType requestType = Kokain::Common::RequestType::Undefined;
    quint8 temp_request_type = 0;
    in >> temp_request_type;
    requestType = static_cast<Kokain::Common::RequestType>(temp_request_type);

    if (requestType == Kokain::Common::RequestType::Connect     ||
        requestType == Kokain::Common::RequestType::Register    ||
        requestType == Kokain::Common::RequestType::Unregister) {
        return requestType;
    }

    return Kokain::Common::RequestType::Undefined;
}
