#-------------------------------------------------
#
# Project created by QtCreator 2015-08-03T21:29:59
#
#-------------------------------------------------

QT       += core gui
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Kokain_Client_Gui
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++0x

SOURCES += main.cpp\
        widget.cpp \
    client.cpp

HEADERS  += widget.h \
    common.h \
    client.h
