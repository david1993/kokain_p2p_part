#include "widget.h"
#include "client.h"

#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QListWidget>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    m_client = new client(this);
    m_register = new QPushButton("Register");
    m_unregister = new QPushButton("Unregister");
    m_connect = new QPushButton("Connect");
    m_send_message = new QPushButton("Send");
    m_messages = new QListWidget;

    m_email_register = new QLineEdit;
    m_email_unregister = new QLineEdit;
    m_email_connect = new QLineEdit;
    m_message = new QLineEdit;

    QHBoxLayout* register_layout = new QHBoxLayout;
    register_layout->addWidget(m_email_register);
    register_layout->addWidget(m_register);

    QHBoxLayout* unregister_layout = new QHBoxLayout;
    unregister_layout->addWidget(m_email_unregister);
    unregister_layout->addWidget(m_unregister);

    QHBoxLayout* connect_layout = new QHBoxLayout;
    connect_layout->addWidget(m_email_connect);
    connect_layout->addWidget(m_connect);

    QHBoxLayout* message_layout = new QHBoxLayout;
    message_layout->addWidget(m_message);
    message_layout->addWidget(m_send_message);

    QVBoxLayout* main = new QVBoxLayout(this);
    main->addLayout(register_layout);
    main->addLayout(unregister_layout);
    main->addLayout(connect_layout);
    main->addLayout(message_layout);
    main->addWidget(m_messages);

    setLayout(main);

    connect(m_register, SIGNAL(clicked()), this, SLOT(onRegister()));
    connect(m_unregister, SIGNAL(clicked()), this, SLOT(onUnregister()));
    connect(m_connect, SIGNAL(clicked()), this, SLOT(onConnect()));
    connect(m_send_message, SIGNAL(clicked()), this, SLOT(onSend()));
    connect(m_client, SIGNAL(new_message()), this, SLOT(onNewMessage()));
}

void Widget::onRegister()
{
    m_client->set_email(m_email_register->text().toStdString());
    m_client->register_request();
}

void Widget::onUnregister()
{
    if (m_email_unregister->text() == QString(""))
        m_client->unregister_request();
    else
        m_client->unregister_request(m_email_unregister->text().toStdString());
}

void Widget::onConnect()
{
    m_client->connection_request(m_email_connect->text().toStdString());
    qDebug() << QString::fromStdString(m_email_connect->text().toStdString());
}

void Widget::onSend()
{
    m_client->send_message(m_email_connect->text().toStdString(), QStringList(m_message->text()));
}

void Widget::onNewMessage()
{
    m_messages->insertItem(0, m_client->get_current_message());
}

Widget::~Widget()
{

}
