#include "client.h"

#include <windows.h>
#include <iostream>

#include <QDebug>

#ifndef min
#define min(a,b) (((a) < (b)) ? (a) : (b))
#endif

client::client(QObject* parent /*= 0*/)
    : QTcpServer(parent),
      m_server_IP(QHostAddress::LocalHost),
      m_server_Port(1234),
      m_server_socket(nullptr)
{
    m_server_socket = new QTcpSocket(this);
    connect(m_server_socket, SIGNAL(connected()), this, SLOT(onConnected()));
    connect(m_server_socket, SIGNAL(readyRead()), this, SLOT(on_ready_read()));
    connect(m_server_socket, SIGNAL(error(QAbstractSocket::SocketError)),this, SLOT(onSokDisplayError(QAbstractSocket::SocketError)));
    m_email = "blablabla1";
}

void client::onSokDisplayError(QAbstractSocket::SocketError socketError)
{
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
        break;
    case QAbstractSocket::HostNotFoundError:
        std::cout << "Error", "The host was not found";
        break;
    case QAbstractSocket::ConnectionRefusedError:
        std::cout << "Error", "The connection was refused by the peer.";
        break;
    default:
        std::cout << "Error", "The following error occurred: " + m_server_socket->errorString();
    }
}

void client::set_email(const std::string &email)
{
    m_email = email;
}

void client::register_request()
{
    if (!m_email.empty())
    {
        qDebug() << QString::fromStdString(m_email);
        m_server_socket->connectToHost(QHostAddress::LocalHost, m_server_Port);
        if (!m_server_socket->waitForConnected(3000)) {
            std::cout << "Can't connect to server" << std::endl;
            return;
        }
        if (m_server_socket->state() == QTcpSocket::ConnectedState)
        {
            m_self_port = m_server_socket->localPort();
            qDebug() << "Self Port" << m_self_port;
        }


        char* data = new char[33];

        data[0] = static_cast<char>(Kokain::Common::RequestType::Register);
        memcpy(data + 1, &m_email[0], min(m_email.size() + 1, 32));
        qDebug() << min(m_email.size() + 1, 32);

        m_server_socket->write(data, 33);
        delete data;
        data = nullptr;
        if (!listen(QHostAddress::Any, m_self_port))
        {
            qDebug() << "Client not started at" << ": " << m_self_port;
        }
        qDebug() << "Client started at" << " : " << m_self_port;
    }
}

void client::unregister_request()
{
    if (!m_email.empty())
    {
        m_server_socket->connectToHost(m_server_IP, m_server_Port);
        if (!m_server_socket->waitForConnected(3000)) {
            std::cout << "Can't connect to server" << std::endl;
            return;
        }

        char* data = new char[33];
        *data = static_cast<unsigned char>(Kokain::Common::RequestType::Unregister);
        memcpy(data + 1, &m_email[0], min(m_email.size() + 1, 32));
        m_server_socket->write(data, 33);

        delete data;
        data = nullptr;
    }
}

void client::unregister_request(const std::string& email)
{
    if (!email.empty())
    {
        m_server_socket->connectToHost(m_server_IP, m_server_Port);
        if (!m_server_socket->waitForConnected(3000)) {
            std::cout << "Can't connect to server" << std::endl;
            return;
        }

        char* data = new char[33];
        *data = static_cast<unsigned char>(Kokain::Common::RequestType::Unregister);
        memcpy(data + 1, &email[0], min(email.size() + 1, 32));
        m_server_socket->write(data, 33);
        delete data;
        data = nullptr;
    }
}

void client::connection_request(const std::string& email)
{
    if (!email.empty())
    {
        qDebug() << "Connect to " << email.size();
        m_server_socket->connectToHost(m_server_IP, m_server_Port);
        if (!m_server_socket->waitForConnected(3000)) {
            std::cout << "Can't connect to server" << std::endl;
            return;
        }

        char* data = new char[33];
        *data = static_cast<unsigned char>(Kokain::Common::RequestType::Connect);
        memcpy(data + 1, &email[0], min(email.size() + 1, 32));
        m_server_socket->write(data, 33);
        delete data;
        data = nullptr;
    }
}

void client::send_message(const std::string& email, const QStringList& message)
{
    qDebug() << QString::fromStdString(email);
    std::string text = message.join(' ').toStdString();
    text = text.substr(0, text.size());
    const auto it = m_connections.find(email);
    if (it == m_connections.end()) {
        qDebug() << "Wrong input";
        return;
    }
    QTcpSocket* connection_socket = get_socket(email);
    char* data = new char[288];
    memcpy(data, &m_email[0], min(m_email.size() + 1, 32));
    memcpy(data + 32, &text[0], min(text.size() + 1, 256));

    if (connection_socket->state() == QTcpSocket::ConnectedState){
        connection_socket->write(QByteArray(data, 288));
        qDebug() << "Connected";
    }
    else {
        std::cout << "Client unconnected \n";
        unregister_request(email);
        m_connections.erase(email);
        m_sockets.erase(email);
    }

    delete data;
    data = nullptr;
}

QString client::get_current_message()
{
    return m_curr_message;
}

QTcpSocket* client::get_socket(const std::string& email) const
{
    const auto it = m_sockets.find(email);
    if (it == m_sockets.end()) {
        std::cout << "Yus@ qyal a\n";
        return nullptr;
    }

    return it->second;
}

void client::incomingConnection(qintptr descriptor)
{
    qDebug() << "Client " << QString::fromStdString(m_email) << "incoming connection";
    QTcpSocket* current_incomming_connection = new QTcpSocket(this);
    current_incomming_connection->setSocketDescriptor(descriptor);
    connect(current_incomming_connection, SIGNAL(readyRead()), this, SLOT(on_ready_read()));
}

void client::on_ready_read()
{
    qDebug() << QString::fromStdString(m_email) << " ready read";
    QTcpSocket* sender = dynamic_cast<QTcpSocket*>(QObject::sender());
    if (!sender) {
        std::cout << "Bad Cast" << std::endl;
        return;
    }

    if (sender->bytesAvailable() == 288)
    {
        qDebug() << "Bytes available == 288";
        QDataStream in(sender);

        std::string     email;
        get_string_from_package(in, email, 32);

        std::string     zrlama;
        get_string_from_package(in, zrlama, 256);

        if (zrlama == std::string("zrlama")) {
            qDebug() << "Zrlama == zrlama";
            Kokain::Common::client curr_client(email, sender->peerAddress(), sender->peerPort());
            m_connections.insert(make_pair(email, curr_client));
            m_sockets.insert(make_pair(email, sender));
            return;
        }

        qDebug() << QString::fromStdString(zrlama);
        m_curr_message = QString::fromStdString(zrlama);
        emit new_message();
    }
    else if (sender->bytesAvailable() == 39) {
        qDebug() << "Package size == 39";
        QDataStream in(sender);

        quint8 exists;
        in >> exists;
        qDebug() << "Exists " << exists;

        if (!exists) {
            qDebug() << "Client not exists \n";
            return;
        }

        std::string     email;
        get_string_from_package(in, email, 32);

        qDebug () << "Bytes Available " << sender->bytesAvailable();

        qDebug () << "Email" << QString::fromStdString(email);

        qint32 ip;
        in >> ip;

        quint16 port;
        in >> port;

        qDebug() << "Port " << port;

        Kokain::Common::client curr_client(email, QHostAddress(ip), port);

        m_connections.insert(make_pair(email, curr_client));
        QTcpSocket* socket = new QTcpSocket(this);
        connect(socket, SIGNAL(readyRead()), this, SLOT(on_ready_read()));
        socket->connectToHost(QHostAddress(ip), port);

        if (!socket->waitForConnected(3000)) {
            qDebug() << "Cant connect";
            unregister_request(email);
            m_connections.erase(email);
            m_sockets.erase(email);
            return;
        }

        qDebug() << "Connected to client " << QString::fromStdString(email) << "\n";
        m_sockets.insert(make_pair(email, socket));

        QString zrlama("zrlama");
        QStringList message;
        message.push_back(zrlama);
        send_message(email, message);
    }
}

void client::onConnected()
{

}

void client::get_string_from_package(QDataStream& in, std::string& message, const size_t max_size)
{
    message.clear();
    qint8 byte = 0;
    char character;
    do {
        in >> byte;
        character = (char)byte;
        qDebug() << character;
        if (character != '\0')
            message.push_back(character);
    } while (character != '\0' && message.size() != max_size);

    int message_size = message.size() + 1;

    while (message_size != max_size)
    {
        ++message_size;
        in >> byte;
    }
}







