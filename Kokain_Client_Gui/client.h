#pragma once

#include <QTcpServer>
#include <QStringList>
#include <QHostAddress>
#include <QTcpSocket>

#include "common.h"

typedef std::map<std::string, Kokain::Common::client> Clients;
typedef std::map<std::string, QTcpSocket*> Sockets;

class client : public QTcpServer
{
    Q_OBJECT
public:
    explicit client(QObject* parent = 0);

public:
    void set_email(const std::string& email);
    void register_request();
    void unregister_request();
    void unregister_request(const std::string& email);
    void connection_request(const std::string& email);
    void send_message(const std::string& email, const QStringList& message);
    QString get_current_message();

private:
    QTcpSocket* get_socket(const std::string& email) const;

private:
    static void get_string_from_package(QDataStream &, std::string&, const size_t max_size);

protected:
    void incomingConnection(qintptr descriptor);

public slots:
    void on_ready_read();
    void onConnected();
    void onSokDisplayError(QAbstractSocket::SocketError socketError);

signals:
    void new_message();

private:
    Clients m_connections;
    Sockets m_sockets;
    std::string m_email;
    QHostAddress m_server_IP;
    quint16 m_server_Port;
    quint16 m_self_port;
    QString m_curr_message;
    QTcpSocket* m_server_socket;
};
