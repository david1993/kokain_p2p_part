#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

class client;
class QPushButton;
class QLineEdit;
class QListWidget;

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void onRegister();
    void onUnregister();
    void onConnect();
    void onSend();
    void onNewMessage();

private:
    client*      m_client;
    QPushButton* m_register;
    QPushButton* m_unregister;
    QPushButton* m_connect;
    QPushButton* m_send_message;
    QLineEdit*   m_message;
    QLineEdit*   m_email_register;
    QLineEdit*   m_email_unregister;
    QLineEdit*   m_email_connect;
    QListWidget* m_messages;
};

#endif // WIDGET_H
