#pragma once

#include <string>
#include <QHostAddress>

namespace Kokain {
namespace Common {

enum class RequestType : char
{
    Undefined = -1,
    Register = 0,
    Connect,
    Unregister
};

struct client
{
    client() {}
    client(const std::string& email,
           const QHostAddress& publicIP,
           const quint16 publicPort)
        : m_email(email),
          m_publicIP(publicIP),
          m_publicPort(publicPort)
    {}

    std::string     m_email;
    QHostAddress    m_publicIP;
    quint16         m_publicPort;
};

struct responce
{
    responce()
    {}

    responce(const bool exists, const std::string& email, const QHostAddress ip, const quint16 port)
        : m_exists(exists),
          m_email(email),
          m_ip(ip),
          m_port(port)
    {}

    bool m_exists;
    std::string m_email;
    QHostAddress m_ip;
    quint16 m_port;
};

}}
